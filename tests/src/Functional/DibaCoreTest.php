<?php

namespace Drupal\Tests\diba_core\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Provides a class for dib_core functional tests.
 *
 * @group diba_core
 */
class DibaCoreTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'diba_core';

  /**
   * {@inheritdoc}
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Tests theme render works and has status code 200.
   */
  public function testsAccess() {
    // Access frontpage with anonymous user.
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);
  }

}
